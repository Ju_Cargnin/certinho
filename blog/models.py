from django.db import models
from django.conf import settings

class Product(models.Model):
	name = models.CharField(max_length=100)
	description = models.TextField()
